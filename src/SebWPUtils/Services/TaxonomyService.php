<?php

namespace SebWPUtils\Services;
use SebWPUtils\Models\HooksInterface;
use \Stringy\Stringy as S;

abstract class TaxonomyService extends Service{
    private $meta;
    //put your code here
    public function load(\WP_Term $term) {
        if ($term->taxonomy != static::type){
            throw new \Exception ('incorrect type ' . $term->taxonomy);
        }
        $this->meta = null;
        $this->item = $term;
    }
    
    public function __get($name)
    {
        if (isset($name, $this->item->$name)) {
            return $this->item->$name;
        }
        if (is_null($this->meta)){
            $this->meta = get_option( "taxonomy_term_". $this->item->term_id );
        }
        if (!$this->meta) return '';
        return isset($this->meta[$name]) ? $this->meta[$name] : '';
    }
    
    public function permalink($page = null){
        // return get_home_url() ."/" . $this->slugprefix . "/" . $this->item->term_id . '/' . S::create($this->item->name)->slugify();
        return get_home_url() ."/" . $this->slugprefix . "/" . $this->item->slug . '/' . $this->item->term_id . 
                (!is_null($page) ? '/page/' . $page: '') ; //. '/page/' . $page; // . '/' . S::create($this->item->name)->slugify();
    }
    
    static public function count(){
        return wp_count_terms(static::type);
    }
    
    public function countPosts(){
        return self::_count([
            'post_type' => static::posttype,
            'post_status'   => 'publish', // just tried to find all published post
            'posts_per_page' => -1,  //show all
            'tax_query' => [
                [
                    'taxonomy' => static::type,
                    'field' => 'term_id',
                    'terms' => $this->item->term_id
                ]
            ]
        ]);
    }
    
}
