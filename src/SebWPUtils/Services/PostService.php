<?php

namespace SebWPUtils\Services;
use SebWPUtils\Models\HooksInterface;

abstract class PostService extends Service{
    //put your code here
    public function load(\WP_Post $item) {
        if ($item->post_type != static::type){
            throw new \Exception ('incorrect type ' . $item->post_type);
        }
        $this->item = $item;
    }
    
    public function __get($name)
    {
        if (isset($name, $this->item->$name)) {
            return $this->item->$name;
        }
        $result = get_post_meta($this->item->ID, $name, true);
        return $result != false ? $result : '';
    }
    
}
