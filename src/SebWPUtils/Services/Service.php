<?php

namespace SebWPUtils\Services;
use SebWPUtils\Models\HooksInterface;

abstract class Service implements HooksInterface{
    static public $instance = [];
    protected $item;
    
    static public function factory(){
        $name = get_called_class(); 
        if (!isset(static::$instance[$name])){
            static::$instance[$name] = new static;
        }
        return static::$instance[$name];
    }
    
    static protected function _doQuery($args){
        $query = new \WP_Query( $args );
        return $query->posts;
    }
    static protected function _count($args){
        $query = new \WP_Query( $args );
        return (int)$query->post_count;
    }
    
    static protected function _doTermQuery($args){
        $query = new \WP_Term_Query( $args );
        return $query->get_terms();
    }
}
