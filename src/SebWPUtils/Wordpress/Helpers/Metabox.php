<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SebWPUtils\Wordpress\Helpers;

use SebWPUtils\Models\HooksInterface;
/**
 * Description of Metabox
 * 
 * https://www.youtube.com/watch?v=bTiAR4kMfwc
 *
 * @author seb
 */
class Metabox {
    public $meta_id;
    public $metabox_title;
    public $fields = [];
    public $post_type;
    public $after_save_callback = null;
    
    public function __construct($type, $fields, $metabox_title = null, $callback = null){
        $this->meta_id = $type . '_meta';
        $this->metabox_title = is_null($metabox_title) ? __('Additional information', 'sebwputils') : $metabox_title;
        $this->post_type = $type;
        $this->after_save_callback = $callback;
        $this->fields = $fields;
        add_action('admin_init', array($this, 'initMetabox'));
        add_action('save_post', array($this, 'savemetas'));
    }
    
    public function initMetabox(){
        add_meta_box( $this->meta_id, $this->metabox_title,  array($this, 'render'), $this->post_type);
    }
    
    public function render()
    {
        echo '<input type="hidden" id="' . $this->meta_id . '_nonce" name="' . $this->meta_id . '_nonce" value="' . wp_create_nonce($this->meta_id) . '" />' . "\n";
        foreach ($this->fields as $field){
            echo $field->render() . "\n";
        }
    }
    
    public function savemetas($post_id){
        if (
           (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) ||
           (defined('DOING_AJAX') && DOING_AJAX)
        ){
            return false;
        }
        if (!current_user_can('edit_post', $post_id)){
            return false;
        }
        if (isset($_POST[$this->meta_id . '_nonce']) && !wp_verify_nonce($_POST[$this->meta_id . '_nonce'], $this->meta_id)){
            return false;
        }
        foreach ($this->fields as $field){
            //echo '; post_id' . $post_id . ' field id ' . $field->id . ' post ' . $_POST[$field->id] . "\n";
            if (isset($_POST[$field->id])){
                $field->save($post_id, sanitize_text_field($_POST[$field->id]));
            }
        }
        if (!is_null($this->after_save_callback)){
            $callable = $this->after_save_callback;
            $this->$callable($post_id);
        }
    }
}
