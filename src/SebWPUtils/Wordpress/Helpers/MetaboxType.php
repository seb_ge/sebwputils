<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SebWPUtils\Wordpress\Helpers;

/**
 * Description of MetaboxType
 *
 * @author seb
 */
abstract class MetaboxType {
    public $id;
    public $title;
    public $default;
    public $additional;
    //put your code here
    public function __construct($id, $title, $default = '', $additional = []){
        $this->id       = $id;
        $this->title    = $title;
        $this->default  = $default;
        $this->additional = $additional;
    }
    
    abstract public function render();
    
    public function save($post_id, $value){
        if (get_post_meta($post_id, $this->id)){
            update_post_meta($post_id, $this->id, $value);
        }else if ($value === ''){
            delete_post_meta($post_id, $this->id);
        }else{
            add_post_meta($post_id, $this->id, $value);
        }
    }
}
