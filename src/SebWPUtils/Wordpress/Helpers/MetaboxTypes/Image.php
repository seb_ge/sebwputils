<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SebWPUtils\Wordpress\Helpers\MetaboxTypes;
use SebWPUtils\Wordpress\Helpers\MetaboxType;
/**
 * Description of Text
 *@deprecated
 * @author seb
 */
class Image extends MetaboxType{
    //put your code here
    public function render(){
        global $post;
        $value = get_post_meta($post->ID, $this->id, true);
        if ($value === ''){
            $value = $this->default;
        }
        $meta = '<div class="meta-box-item-title"><h4>' . $this->title .
                '</h4></div><div class="meta-box-item-content">';
        if (!empty($value)){
           $meta .= '<img src="' . $value . '" style="max-width:200px; max-height:200px; display:block" />';
        }
        return $meta . '<input id="' . $this->id . '" type="text" name="' . $this->id . '" value="' . $value . 
                '" /> <a id="' . $this->id . '_btn" href="#" class="button journal-js-uploader" data-multiple="false" ' .
                'data-id="' . $this->id . '">' . _('Charger', SIMPLE_JOURNAL_TRANSLATECST) . '</a></div>';
    }
}
