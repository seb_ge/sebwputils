<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SebWPUtils\Wordpress\Helpers\MetaboxTypes;
use SebWPUtils\Wordpress\Helpers\MetaboxType;
/**
 * Description of Text
 *
 * @author seb
 */
class Text extends MetaboxType{
    //put your code here
    public function render(){
        global $post;
        $value = get_post_meta($post->ID, $this->id, true);
        if ($value === ''){
            $value = $this->default;
        }
        return '<div class="meta-box-item-title"><h4>' . $this->title .
                '</h4></div><div class="meta-box-item-content">' . 
                '<input id="' . $this->id . '" type="text" name="' . $this->id . 
                '" value="' .  $value . '" /></div>';
    }
}
