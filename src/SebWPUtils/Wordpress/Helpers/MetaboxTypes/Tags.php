<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace SebWPUtils\Wordpress\Helpers\MetaboxTypes;
use SebWPUtils\Wordpress\Helpers\MetaboxType;
/**
 * Description of Text
 * @deprecated 
 * @author seb
 */
class Tags extends MetaboxType{
    //put your code here
    public function render(){
        global $post;
        $values = apply_filters($this->additional['getterhook'], $post->ID);
        return '<hr/><div class="meta-box-item-title"><h4>' . $this->title . '</h4></div>'
                . '<div class="panel panel-default" style="padding-left:0px">
 		 <div class="panel-body" id="' . $this->id . '_list" style="padding:5px">
		</div></div><p class="help_block">' . _('Click on x to remove item, move with mouse to change order', SIMPLE_JOURNAL_TRANSLATECST) . '</p>'
                . '<div class="meta-box-item-content">' . 
                '<input id="' . $this->id . '_enter" type="text" name="' . $this->id . 
                '_enter" value="" autocomplete="off" placeholder="' . $this->title . '"/>' . 
                '<p class="help_block">' . _('Type search item, select item with arrows and validate with "Enter" key or use mouse.', SIMPLE_JOURNAL_TRANSLATECST) . 
                (isset($this->additional['additionalhelper']) ? ' ' . $this->additional['additionalhelper'] : '') . '</p></div>' .
                 '<input class="tagstosave" id="' . $this->id . '" type="hidden" name="' . $this->id . 
                '" value="" />' . 
                "<script>\n"
                . $this->id ."_save = function(){
                    jQuery('#" . $this->id ."').val(jQuery('#" . $this->id . "_list').data('mytags').getValues());
                }\n"
                . "jQuery('#" . $this->id . "_list').mytags({style:'" . $this->additional['tag-label']. "', deletecallback: " .
                    $this->id ."_save});\n"
                . "var values = " . $values . ";\njQuery.each(values, function( index, value ) {\n"
                . "     jQuery('#" . $this->id . "_list').data('mytags').add(value);\n"
                . "});\n"
                . "jQuery('#" . $this->id . "_enter').autocomplete({
                    serviceUrl: '" . $this->additional['url'] . "',
                    type : 'POST',
                    minChars : " . (isset($this->additional['minchars']) ?  $this->additional['minchars'] : 3) . ",
                    preventBadQueries : false,
                    dataType : 'json',
                    paramName : 'search',
                    params : {
                        ajax_nonce  : jQuery('input[name=" . $this->additional['nonce'] . "]').val(),
                        action : '" . $this->additional['action'] . "'
                    },
                    onSelect: function (suggestion) {
                        jQuery('#" . $this->id . "_list').data('mytags').add({value: suggestion.data, label: suggestion.value});
                        jQuery('#" . $this->id . "_enter').val('');
                         ". $this->id ."_save();   
                    }
                   });"
                . "\n</script><hr/>";
    }
    
    public function save($post_id, $value){
        do_action($this->additional['hook'], $post_id, $value);
    }
}