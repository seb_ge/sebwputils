<?php

/*
 probably deprecated
 */

namespace SebWPUtils\Wordpress\Helpers\MetaboxTypes;
use SebWPUtils\Wordpress\Helpers\MetaboxType;
/**
 * Description of Text
 * @deprecated
 * @author seb
 */
class AddItem extends MetaboxType{
    //put your code here
    public function render(){
        $meta = '<div class="meta-box-item-title"><h4>' . $this->title . '</h4></div>' . "\n" .
                '<div class="meta-box-item-content"><form class="form-inline" id="' . $this->id . '">' . "\n";
        foreach ( $this->additional['inputs'] as $input){
             $meta .= '<input type="text" class="form-control" id="' . $input['id']. '" placeholder="' . $input['text'] . '"/>';
        }
        $meta .= '<input type="button" id="' . $this->id . '_send" class="btn btn-default" value="' . $this->additional['submitbtntext'] .'"/></form></div>';
        $meta .= "<script>jQuery( document ).ready(function( jQuery ) {"
                . "jQuery('#" . $this->id . "_send').bind('click', function(e){"
                . "  e.preventDefault();
                    jQuery.ajax({ 
                        url: '/wp-admin/admin-ajax.php',
                        cache: false,
                        encoding : 'utf8',
                        type : 'post',
                        data : {
                            'action':'" . $this->additional['ajaxaction'] . "',
                            'ajax_nonce': '" . wp_create_nonce($this->additional['ajaxaction']) . "',";
        foreach ( $this->additional['inputs'] as $input){
             $meta .= "'" . $input['id'] . "' : jQuery('#" . $input['id'] . "').val(),";
        }                    
        $meta .= "     },
                        success : function(res){
                            if (res.ok){
                                jQuery('#" . $this->additional['list_prefix'] . "_list').data('mytags').add({'value' : res.values.id, 'label' : res.values.lastname + ' ' + res.values.firstname});
                            }
                        }
                    });"
                . "});\n});"
                . "</script>";
        return $meta; 
    }
}
