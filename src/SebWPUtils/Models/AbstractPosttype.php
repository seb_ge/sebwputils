<?php
namespace SebWPUtils\Models;

use Stringy\Stringy as S;
/**
 * Description of AbstractPosttype
 *
 * @author seb
 */
abstract class AbstractPosttype implements HooksInterface{
    protected $type;
    protected $slugprefix;
    
    ///abstract public function addtoslug();
    protected function init_rewrites(){
        add_action('generate_rewrite_rules', array(&$this, 'generating_rule'));
        add_filter('query_vars', array(&$this, 'insert_query_vars') );
        add_filter('post_type_link', array(&$this, 'change_link'), 10, 2);
    }
    
    abstract public function generating_rule($wp_rewrite);
    
    abstract function change_link($permalink, $post);
    

// Adding the id var so that WP recognizes it
    public function insert_query_vars( $vars )
    {
        array_push($vars, 'id');
        return $vars;
    }
}
