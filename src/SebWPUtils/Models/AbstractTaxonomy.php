<?php
namespace SebWPUtils\Models;

use SebWPUtils\Models\HooksInterface;
/**
 * Description of AbstractTaxonomy
 *
 * @author seb
 */
abstract class AbstractTaxonomy implements HooksInterface{
    protected $slugprefix;
    protected $type;
    protected $posttype;
    
    abstract public function initTaxonomy();
    
    protected function init_rewrites(){
        add_action('generate_rewrite_rules', array(&$this, 'generating_rule'));
        add_action('parse_query', array(&$this, 'change_query'));
        add_filter('query_vars', array(&$this, 'insert_query_vars') );
       // add_filter('post_type_link', array(&$this, 'change_link'), 10, 2);
    }
    
    function change_query( $query ) {
       //echo 'chgq '; var_dump($query);
        if( isset( $query->query_vars[$this->slugprefix] ) ){
            if( $term = get_term_by( 'id', $query->query_vars[$this->slugprefix], $this->type ) ){
                $query->query_vars['term'] = $term->slug;
            }
        }
    }
    
    //abstract function change_link($permalink, $post);
    public function generating_rule($wp_rewrite) {
        $rules = array();
        
        // \/issue\/(\d*)(\/.*)?
        $rules[$this->slugprefix  . '/([^/]*)/([0-9]{1,})$'] = 'index.php?' . $this->type . '=$matches[1]&' . $this->slugprefix . 
                '=$matches[2]';
        $rules[$this->slugprefix  . '/([^/]*)/([0-9]{1,})/page/([0-9]{1,})$'] = 'index.php?' . $this->type . '=$matches[1]&' . $this->slugprefix . 
                '=$matches[2]&paged=$matches[3]';
        $wp_rewrite->rules = $rules + $wp_rewrite->rules;
    }

// Adding the id var so that WP recognizes it
    public function insert_query_vars( $vars )
    {
        array_push($vars, $this->slugprefix);
        return $vars;
    }
}
